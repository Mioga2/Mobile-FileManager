var MiogaFileManager = Backbone.Model.extend({

	mioga: null,

	// Browsing
	path: null,
	nodes: null,

	// Copy/cut/paste
	pasteMode: null,
	pasteFiles: null,

	// Create file
	fileType: null,
	fileToCreate: null,

	// Delete files
	deleteFiles: null,
	
	initialize: function(args) {
		this.mioga = args.mioga;

		var that = this;

		this.bind('change:path', function() { that.getFullNodes(); });
		this.bind('change:pasteFiles', function() { that.pasteResources(); });
		this.bind('change:fileToCreate', function() { that.createResource(); });
		this.bind('change:deleteFiles', function() { that.deleteResources(); });
	},

	pasteResources: function() {
		if (this.get('pasteFiles') != null) {
			var that = this;

			$.ajax({
				type: 'POST',
				crossDomain: true,
				data: {
					clipboard: true,
					entries: that.get('pasteFiles'),
					destination: that.get('path'),
					mode: that.get('pasteMode')
				},
				xhrFields: { withCredentials: true },
				traditional: true,
				url: that.mioga.get('private_bin_uri') + '/Magellan/PasteResources.json',
				success: function() {
					that.set('pasteFiles', null);
				},
				error: function(error) {					
					alert('An error occured : ' + error);
				}
			});
		}
	},

	createResource: function() {
		if (this.get('fileToCreate') != null) {
			var that = this;

			$.ajax({
				type: 'POST',
				url: that.mioga.get('private_bin_uri') + '/Magellan/CreateResource.json',
				crossDomain: true,
				data: {
					name: that.get('fileToCreate'),
					path: that.get('path'),
					type: that.get('fileType')
				},
				xhrFields: { withCredentials: true },
				success: function() {
					that.set('fileToCreate', null);
				},
				error: function(error) {
					alert('An error occured : ' + error);
				}
			});
		}
	},

	getFullNodes: function() {
		var that = this;

		$.ajax({
			type: 'POST',
			url: this.mioga.get('private_bin_uri') + '/Magellan/GetFullNodes.json',
			crossDomain: true,
			xhrFields: { withCredentials: true },
			data: { files: 1, node: that.get('path') },
			success: function(data) {
				that.set('nodes', data); 
			},
			error: function (error) { 
				alert('An error occured in getFullNodes: ' + error); 
			}
		});
	},

	deleteResources: function() {
		if (this.get('deleteFiles') != null) {
			var that = this;

			$.ajax({
				type: 'POST',
				crossDomain: true,
				data: { entries: that.get('deleteFiles') },
				traditional: true,
				xhrFields: { withCredentials: true },
				url: that.mioga.get("private_bin_uri") + "/Magellan/DeleteResources.json",
				success: function(data) {
					that.set('deleteFiles', null);
				},
				error: function(error) {
					alert('An error occured : ' + error);
				}
			});
		}
	},

	downloadResources: function(entries) {
		if (entries !== undefined && entries.length > 0) {
			var url = this.mioga.get("private_bin_uri") + "/Magellan/DownloadFiles?path=" + encodeURIComponent(entries[0].substr(0, entries[0].lastIndexOf("/")));
			for (var i = 0; i < entries.length; ++i) {
				url += "&entries=" + encodeURIComponent(entries[i]);
			}
			window.location = url;
		}
	}

});
