var MiogaLogin = Backbone.View.extend({

	$el: null,
	mioga: null,

	initialize: function(args) {
		console.log('[MiogaLogin::initialize]');

		var that = this;
		this.$el = args.$el;
		this.mioga = args.mioga;

		// Register routes for module
		this.mioga.router.route ('login', function() {
			console.log('[MiogaLogin::event] User is not authenticated, displaying login form');

			$('body').removeClass('main');

			// Load template
			var template = Handlebars.compile($('#login-form').html());

			// Render template
			var $node = $(template());
			that.$el.empty().append($node);
		});

		this.mioga.router.route('logout', function() {
			console.log('[MiogaLogin::event] Caught logout request');
			$.ajax ({
				type: 'GET',
				url: that.mioga.get('logout_uri'),
				crossDomain: true,
				xhrFields: { withCredentials: true },
				success: function(data) {
					that.mioga.set('logged', false);
				}
			});
		});

		this.mioga.bind('change:logged', function() {
			if (that.mioga.get('logged') === false) {
				// User logged out, display login form
				that.mioga.router.navigate('login', { trigger: true });
			} else {
				// User logged in, display main application view
				that.mioga.router.navigate('filemanager', { trigger: true });
			}
		});
	},

	events: {
		'click button': function() {
			console.log ('[MiogaLogin::events] Button click');

			var that = this;

			$.ajax({
				type: 'POST',
				url: that.mioga.get('login_uri'),
				crossDomain: true,
				xhrFields: { withCredentials: true },
				data: {
					login: $('input[name=login]').val(),
					password: $('input[name=password]').val()
				},
				success: function(data) {
					// Login system doesn't speak JSON yet.
					// Checking if logged successfully can only be achieved through analyzing response output trying to find out if login form is present (unsuccessful) or not (successful).
					if (data.match(/<body.*login-form/)) {
						alert('The login information entered is not correct');
						that.mioga.set('logged', false);
					}
					else {
						that.mioga.set('logged', true);
					}
				},
				error: function() {
					alert('Connection error');
					// Server returned an error, consider login unsuccessful
					that.mioga.set('logged', false);
				}
			});

			return (false);
		}
	}

});

