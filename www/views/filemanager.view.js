var MiogaFileManagerView = Backbone.View.extend({

	$el: null,
	selectMode: false,
	selectedFiles: [],
	clipboard: [],

	initialize: function(args) {
		console.log ('[MiogaFileManagerView::initialize]');

		this.$el = args.$el;

		Handlebars.registerHelper('icon', function(key) {
			var iconCode = '';

			if (key == 'directory') iconCode = 'fa-folder-o';
			else if (key.indexOf('text') > -1) iconCode = 'fa-file-text-o';
			else if (key.indexOf('image') > -1) iconCode = 'fa-file-image-o';
			else if (key.indexOf('word') > -1) iconCode = 'fa-file-word-o';
			else iconCode = 'fa-question';

			return new Handlebars.SafeString('<i class="fa ' + iconCode + ' fa-3x"></i>');
		});

		Handlebars.registerHelper('rightsString', function(key) {
			var rights = '';

			if (key.indexOf('r') > -1) 
				rights = rights.concat('Lecture');

			if (key.indexOf('w') > -1) 
				rights = rights.concat(', Ecriture');

			return rights;
		});

		Handlebars.registerHelper('dateString', function(key) {
			var d = new Date(key.substring(0, key.indexOf(' ')));
			return 'Modifié le ' + d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear();
		});

		var that = this;

		this.model.bind('change:nodes', function() { that.renderNodes(); });
		this.model.bind('change:pasteFiles', function() { that.afterPaste(); });
		this.model.bind('change:fileToCreate', function() { that.afterCreate(); });
		this.model.bind('change:deleteFiles', function() { that.afterDelete(); });

		// Register routes for module
		this.model.get('mioga').router.route('filemanager', function() {
			$('body').addClass('main');

			$('#btnBack').on('click', function() {
				that.exitSelectMode();
				var path = that.model.get('path');
				path = path.substring(0, path.lastIndexOf('/'));
				that.navigate(path);
			});

			$('#btnLogout').on('click', function() {
				that.model.get('mioga').router.navigate('logout', { trigger: true });
			});

	 		$('#newFile').on('click', function() {
				var template = Handlebars.compile($('#newfile-popup').html());
				$('body').append(template);
				$('#popupNewFile').show();

				$('#createFileBtn').on('click', function() {
					$('.loader').show();
					that.model.set('fileType', $('input[type="radio"][name="fileType"]:checked').val());
					that.model.set('fileToCreate', $('#fileName').val());
					$('#popupNewFile').remove();
				});

				$('.exit').on('click', function() {
					$('#popupNewFile').remove();
				});
			});

			$('#cut, #copy, #paste, #delete, #download').css('opacity', '0.5');

			that.navigate(that.model.get('mioga').get('home_uri'));
		});
	},

	navigate: function(path) {
		$('.loader').show();

		if (typeof path !== 'undefined') {
			$('#headerText').val(path);
			this.model.set('path', path);
		} else {
			// Trigger the change event even if the value did not change
			this.model.trigger('change:path', this.model);
		}
	},

	selectFile: function(el) {
		this.selectMode = true;

		if (this.selectedFiles.indexOf(el.id) > -1) {
			$(el).css('opacity', '1');
			var index = this.selectedFiles.indexOf(el.id);
			this.selectedFiles.splice(index, 1);
		} else {
			$(el).css('opacity', '0.5');
			this.selectedFiles.push(el.id);
		} 
		
		this.bindIcons();
	},

	bindIcons: function() {
		if (this.selectedFiles.length == 0) {
			this.exitSelectMode();
		} else {
			var that = this;

			$('#cut, #copy, #delete, #download').off().css('opacity', '1');
			$('#cut').on('touchstart', function() { that.copyResources(true); });
			$('#copy').on('touchstart', function() { that.copyResources(false); });
			$('#download').on('touchstart', function() { that.model.downloadResources(that.selectedFiles); });

			$('#delete').on('touchstart', function() {
				var template = Handlebars.compile($('#yesno-popup').html());
				$('body').append(template);
				$('#popupYesNo').show();

				$('#yesBtn').on('click', function() {
					$('.loader').show();
					that.model.set('deleteFiles', that.selectedFiles);
					$('#popupYesNo').remove();
				});

				$('#noBtn, .exit').on('click', function() {
					$('#popupYesNo').remove();
				});
			});
		}
	},

	resetSelectedFiles: function() {
		$('section.view li').css('opacity', '1');
		this.selectedFiles = [];
	},

	copyResources: function(cut) {
		this.clipboard = this.selectedFiles;

		$('#paste').css('opacity', '1');

		var that = this;
		$('#paste').off().on('click', function() {
			$('.loader').show();
			that.model.set('pasteMode', (cut ? 'cut' : 'copy'));
			that.model.set('pasteFiles', that.clipboard);
		});
	},

	afterPaste: function() {
		if (this.model.get('pasteFiles') == null) {
			if (this.model.get('pasteMode') == 'cut') {
				$('#paste').css('opacity', '0.5');
				$('#paste').off();
				this.clipboard = [];
			}
			// Refresh
			this.navigate();
		}
	},

	afterCreate: function() {
		if (this.model.get('fileToCreate') == null) {
			this.navigate();
		}
	},

	afterDelete: function() {
		if (this.model.get('deleteFiles') == null) {
			this.exitSelectMode();
			this.navigate();
		}
	},

	renderNodes: function() {
		// Load template
		var template = Handlebars.compile($('#filemanager-form').html());

		// Render template
		var $node = $(template(this.model.get('nodes')));
		this.$el.empty().append($node);

		var timer;
		var longpress = false;

		var that = this;

		$('section.view li').on('touchstart', function() {
			var el = this;
			timer = setTimeout(function() {
			longpress = true;

				if (!that.selectMode) {
					$('#quit').show();
					$('#quit').on('touchstart', function() {
						that.exitSelectMode();
					});
				}

				that.selectFile(el);
			}, 1000);
		}).on('touchend', function() {   
			clearTimeout(timer);
			longpress = false;
			that.bindIcons();
		}).on('click', function() {
			if (!longpress) {
				if (!that.selectMode) {
					if (this.className == 'directory') {
						that.navigate(this.id);
					} else {
						var el = this;

						var template = Handlebars.compile($('#choice-popup').html());
						$('body').append(template({
							"author": $(el).attr("data-author"),
							"group": $(el).attr("data-group"),
							"mimetype": $(el).attr("data-mimetype"),
							"size": $(el).attr("data-size")
						}));

						$('#popupFile').show();

						$('#btnDiv').show();
						$('#aboutDiv').hide();

						$('#downloadBtn').on('click', function() {
							that.model.downloadResources([el.id]);
							$('#popupFile').remove();
						});

						$('#aboutBtn').on('click', function() {
							$('#aboutDiv').show();
							$('#btnDiv').hide();

							$('#popupBack').on('click', function() {
								$('#aboutDiv').hide();
								$('#btnDiv').show();
							});
						});

						$('.exit').on('click', function() {
							$('#popupFile').remove();
						});
					}
				} else {
					that.selectFile(this);
				}
			}
		});

		$('.loader').hide();
	},

	exitSelectMode: function() {
		this.selectMode = false;

 		$('#quit').hide();

		if (this.clipboard.length == 0) {
			$('#paste').off().css('opacity', '0.5');
		}

		$('#cut, #copy, #delete, #download').off().css('opacity', '0.5');
	
		this.resetSelectedFiles();
	}

});
